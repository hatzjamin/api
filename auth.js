// We require the jsonwebtoken module and then contain it in jtw variable.
const jwt = require("jsonwebtoken");

// used in algortihm for encrypting our data which makes it difficult to decode the information without defined secret key.
const secret = "ProductAPI";

/*[Section] JSON Web token*/

// Token creation
/*
	Analogy:
		Pack the gift provided with a lock, which can only be opened using the secret code as the key.
*/

module.exports.createAccessToken = (result) =>{
	// payload of the JWT
	const data = {
		id: result._id,
		email: result.email,
		isAdmin: result.isAdmin
	}

	// Generate a JSON web token using the jwt's sign method.
		// Syntax:
			// jwt.sign(payload, secretOrPrivateKey, [callbackfunction])

	return jwt.sign(data, secret, {});

}

/*Token Verification*/

/*
	-Analogy
		receive the gift and open the lock to verify if the sender is legitimate and the gift was not tamper with
*/

// verification
module.exports.verify = (request, response, next) => {
	let token = request.headers.authorization;
	console.log("@TOKEN", token);
	if (token !== undefined) {
		token = token.slice(7, token.length);
		console.log(token);

		return jwt.verify(token, secret, (error) => {
			let isTokenValid;
			if (error){
				response.send({isTokenValid: false});
			} else {
				next();
			}
		})
	}
	else {
		let isTokenProvided;
		return response.send({isTokenProvided: false});
	}
}


// Token decryption
/*
	-Analogy:
		Opening gift or unwrapping presents
*/

module.exports.decode = (token) => {
	if(token === undefined){
		return null
	}
	else{
		token = token.slice(7, token.length);

		return jwt.verify(token, secret, (error, data) => {
			if(error){
				return null;
			}else{
				// decode method is used to obtaine the information from the JWT.
				// Syntax: jwt.decode(token, [options])
				// Return an object with the access to the payload property.
				return jwt.decode(token, {complete: true}).payload;
			}
		})
	}
}